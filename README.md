This repository hosts in-house libraries that are developed to customize [DSpace](http://dspace.org/) for the need of the **Treasury of Wisdom** LL archive project. 

## Use it with **maven**

In your `pom.xml`, add the following `repository`:
  
```
<repositories>
    <repository>
        <id>tow-maven-repo</id>
        <name>Treasury of Wisdom Maven Repository</name>
        <url>https://gitlab.com/rigpa/software/tow/mvn/-/raw/master/</url>
        <layout>default</layout>
    </repository>
</repositories>
```